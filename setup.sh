#!/bin/bash

#core development environment
#     sudo ssh git emacs
#javascript development
#     phantomjs
#additional development dependencies
#     python2 base-devel (for compiling certain npm modules, like serialport
pacman -Sy --noconfirm sudo openssh emacs-nox git python2 base-devel screen avahi nss-mdns

#configure hostname resolution
sed -i 's/^hosts:.*/hosts: files myhostname mdns_minimal [NOTFOUND=return] dns/' /etc/nsswitch.conf
#enable services
systemctl enable sshd
systemctl enable avahi-daemon.service  

systemctl start sshd
systemctl start avahi-daemon.service  

#set hostname
echo -n "hostname:"
read hostname < /dev/tty
echo "$hostname" > /etc/hostname

#create new user
echo -n "Local user:"
read username < /dev/tty

x=0
while [ $x -eq 0 ]
do
    echo -n "Password:"
    read password < /dev/tty
    echo -n "Confirm password:"
    read passwordConfirmation < /dev/tty
    if [ $password = $passwordConfirmation ]
    then
        x=1
    else
        echo "Passwords do not match!"
    fi
done

echo ""
echo "Creating user $username"

useradd -m -g users -s /bin/bash $username
echo "Setting password for $username"
echo "$username:$password"|/usr/bin/chpasswd

su $username <<'EOF'

#change to home directory
cd ~

#setup custom dotfiles
curl https://bitbucket.org/yianisn/archsetup/raw/master/.bash_aliases > ~/.bash_aliases
curl https://bitbucket.org/yianisn/archsetup/raw/master/.screenrc > ~/.screenrc

mkdir .emacs.d
curl https://bitbucket.org/yianisn/archsetup/raw/master/.emacs.d/init.el > ~/.emacs.d/init.el

#source aliases in .bash_profile
if [ -f ".bash_profile" ]; then
  PROFILE=".bash_profile"
fi

SOURCE_STR=" . ~/.bash_aliases"

if ! grep -qc '.bash_aliases' $PROFILE; then
  echo "=> Appending aliases source to $PROFILE"
  echo "" >> "$PROFILE"
  echo $SOURCE_STR >> "$PROFILE"
else
  echo "=> Aliases source already in $PROFILE"
fi

EOF


