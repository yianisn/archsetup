#!/bin/bash

#configure proxy
echo -n "Do you need to use a proxy? (yes/no)"
read proxyPresent < /dev/tty
if [ $proxyPresent = "yes" ] 
then
    echo -n "http_proxy?"
    read httpProxy < /dev/tty
    echo -n "https_proxy?"
    read httpsProxy < /dev/tty

    export http_proxy=$httpProxy
    export https_proxy=$httpsProxy

    #disable package signature checking when behind a proxy
    echo "SigLevel = Never" >> /etc/pacman.conf

    #change keyserver to be usable behind the proxy
    sed -i 's/^keyserver .*/keyserver hkp:\/\/keyserver.kjsl.com:80/' /etc/pacman.d/gnupg/gpg.conf
fi

pacman -Sy --noconfirm archlinux-keyring

#core development environment
#     sudo ssh git emacs
#javascript development
#     phantomjs
#additional development dependencies
#     python2 base-devel (for compiling certain npm modules, like serialport
pacman -S --noconfirm sudo openssh emacs-nox git phantomjs python2 base-devel screen avahi nss-mdns

#configure hostname resolution
sed -i 's/^hosts:.*/hosts: files myhostname mdns_minimal [NOTFOUND=return] dns/' /etc/nsswitch.conf
#enable services
systemctl enable sshd
systemctl enable avahi-daemon.service  

systemctl start sshd
systemctl start avahi-daemon.service  

#set hostname
echo -n "hostname:"
read hostname < /dev/tty
echo "$hostname" > /etc/hostname

#create new user
echo -n "Local user:"
read username < /dev/tty

x=0
while [ $x -eq 0 ]
do
    echo -n "Password:"
    read password < /dev/tty
    echo -n "Confirm password:"
    read passwordConfirmation < /dev/tty
    if [ $password = $passwordConfirmation ]
    then
        x=1
    else
        echo "Passwords do not match!"
    fi
done

echo ""
echo "Creating user $username"

useradd -m -g users -s /bin/bash $username
echo "Setting password for $username"
echo "$username:$password"|/usr/bin/chpasswd

echo "Adding $username to sudoers"
echo "$username ALL=(ALL:ALL) ALL" >> /etc/sudoers

if [ $proxyPresent = "yes" ]
then
    echo "export http_proxy=$httpProxy" > /home/${username}/.bash_proxy
    echo "export https_proxy=$httpsProxy" >> /home/${username}/.bash_proxy
    chown $username:users /home/${username}/.bash_proxy
fi

su -l $username <<'EOF'

#change to home directory
cd ~

PROXY_FILE=".bash_proxy"
if [ -f $PROXY_FILE ]; then
  source $PROXY_FILE
fi

#install nvm
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | sh

# Load nvm and install latest production node
source ~/.nvm/nvm.sh
nvm install 0.10
nvm use 0.10
nvm alias default 0.10

#setup custom dotfiles
curl https://bitbucket.org/yianisn/archsetup/raw/master/.bash_aliases > ~/.bash_aliases
curl https://bitbucket.org/yianisn/archsetup/raw/master/.screenrc > ~/.screenrc

mkdir .emacs.d
curl https://bitbucket.org/yianisn/archsetup/raw/master/.emacs.d/init.el > ~/.emacs.d/init.el

#source additional files in .bash_profile
PROFILE_FILE=".bash_profile"
if [ -f $PROFILE_FILE ]; 
then

  ALIAS_FILE=".bash_aliases"

  if ! grep -qc $ALIAS_FILE $PROFILE_FILE; 
  then
    echo "=> Appending aliases source to $PROFILE"
    echo "" >> "$PROFILE_FILE"
    SOURCE_STR=" . ~/$ALIAS_FILE"
    echo $SOURCE_STR >> "$PROFILE_FILE"
  else
    echo "=> Aliases source already in $PROFILE_FILE"
  fi

  #source proxy in .bash_profile
  if [ -f $PROXY_FILE ]; 
  then
    if ! grep -qc $PROXY_FILE $PROFILE_FILE; 
    then
      echo "=> Appending proxy source to $PROFILE_FILE"
      echo "" >> "$PROFILE_FILE"
      SOURCE_STR=" . ~/$PROXY_FILE"
      echo $SOURCE_STR >> "$PROFILE_FILE"
    else
      echo "=> Proxy already in $PROFILE_FILE"
    fi
  fi
fi

EOF
